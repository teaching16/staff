@extends('layouts.master')

@section('content')
<h1>{{__('I love programming.')}}</h1>
<div class="row mt-3">
    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Users</span>
                <span class="info-box-number">
                    {{$users}}
                    <small>នាក់</small>
                </span>
            </div>

        </div>

    </div>

    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Employees</span>
                <span class="info-box-number">{{$employees}}</span>
            </div>

        </div>

    </div>

    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">New Employees</span>
                <span class="info-box-number">{{$new_employees}}</span>
            </div>

        </div>

    </div>
    <div class="clearfix hidden-md-up"></div>
    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Sales</span>
                <span class="info-box-number">760</span>
            </div>

        </div>

    </div>
</div>

<div class="row mt-3">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                    <h3 class="card-title">Users By Role</h3>
                    <!-- <a href="javascript:void(0);">View Report</a> -->
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex">
                    <p class="d-flex flex-column">
                        <span class="text-bold text-lg">{{$saleChartTotal}} នាក់</span>
                        <!-- <span>Sales Over Time</span> -->
                    </p>
                    <!-- <p class="ml-auto d-flex flex-column text-right">
                        <span class="text-success">
                            <i class="fas fa-arrow-up"></i> 33.1%
                        </span>
                        <span class="text-muted">Since last month</span>
                    </p> -->
                </div>

                <div class="position-relative mb-4">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <canvas id="sales-chart" height="400" style="display: block; height: 200px; width: 532px;"
                        width="1064" class="chartjs-render-monitor"></canvas>
                </div>
                <div class="d-flex flex-row justify-content-end">
                    <span class="mr-2">
                        <i class="fas fa-square text-primary"></i> All
                    </span>
                    <!-- <span>
                        <i class="fas fa-square text-gray"></i> Female
                    </span> -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                    <h3 class="card-title">Online Store Visitors</h3>
                    <a href="javascript:void(0);">View Report</a>
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex">
                    <p class="d-flex flex-column">
                        <span class="text-bold text-lg">820</span>
                        <span>Employees</span>
                    </p>
                    <!-- <p class="ml-auto d-flex flex-column text-right">
                        <span class="text-success">
                            <i class="fas fa-arrow-up"></i> 12.5%
                        </span>
                        <span class="text-muted">Since last week</span>
                    </p> -->
                </div>

                <div class="position-relative mb-4">
                    <!-- <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div> -->
                    <canvas id="visitors-chart" height="400" width="1064"
                        style="display: block; height: 200px; width: 532px;" class="chartjs-render-monitor"></canvas>
                </div>
                <div class="d-flex flex-row justify-content-end">
                    <!-- <span class="mr-2">
                        <i class="fas fa-square text-primary"></i> This Week
                    </span>
                    <span>
                        <i class="fas fa-square text-gray"></i> Last Week
                    </span> -->
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                    <h3 class="card-title">Users By Role</h3>
                    <!-- <a href="javascript:void(0);">View Report</a> -->
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex">
                    <p class="d-flex flex-column">
                        <span class="text-bold text-lg">{{$saleChartTotal}} នាក់</span>
                        <!-- <span>Sales Over Time</span> -->
                    </p>
                    <!-- <p class="ml-auto d-flex flex-column text-right">
                        <span class="text-success">
                            <i class="fas fa-arrow-up"></i> 33.1%
                        </span>
                        <span class="text-muted">Since last month</span>
                    </p> -->
                </div>

                <div class="position-relative mb-4">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <canvas id="employee-by-gender" height="400" style="display: block; height: 200px; width: 532px;"
                        width="1064" class="chartjs-render-monitor"></canvas>
                </div>
                <div class="d-flex flex-row justify-content-end">
                    <span class="mr-2">
                        <i class="fas fa-square text-primary"></i> Male
                    </span>
                    <span>
                        <i class="fas fa-square text-gray"></i> Female
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<!-- <script src="https://adminlte.io/themes/v3/plugins/chart.js/Chart.min.js"></script> -->
<!-- <script src="https://adminlte.io/themes/v3/dist/js/pages/dashboard3.js"></script> -->
<script src="{{asset('assets/plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('assets/dist/js/pages/dashboard3.js')}}"></script>

<script>
$(document).ready(function() {
    // active menu 
    $(".sidebar li a").removeClass("active");
    $("#menu_dashboard").addClass("active");
})
"use strict";

var ticksStyle = {
    fontColor: "#495057",
    fontStyle: "bold"
};
var mode = "index";
var intersect = true;
var $salesChart = $("#sales-chart");

// ================== role =================//
var salesChartData = <?php echo $saleChartData ?>;
var salesChart = new Chart($salesChart, {
    type: "bar",
    data: {
        labels: salesChartData.label,
        datasets: [{
                backgroundColor: "#007bff",
                borderColor: "#007bff",
                data: salesChartData.data,
            },
            // {
            //     backgroundColor: "#ced4da",
            //     borderColor: "#ced4da",
            //     data: [700, 1700, 2700, 2000, 1800, 1500, 2000],
            // },
        ],
    },
    options: {
        maintainAspectRatio: false,
        tooltips: {
            mode: mode,
            intersect: intersect
        },
        hover: {
            mode: mode,
            intersect: intersect
        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                gridLines: {
                    display: true,
                    lineWidth: "4px",
                    color: "rgba(0, 0, 0, .2)",
                    zeroLineColor: "transparent",
                },
                ticks: $.extend({
                        beginAtZero: true,
                        callback: function(value) {
                            if (value >= 1000) {
                                value /= 1000;
                                value += "k";
                            }
                            return "នាក់" + value;
                        },
                    },
                    ticksStyle
                ),
            }, ],
            xAxes: [{
                display: true,
                gridLines: {
                    display: false
                },
                ticks: ticksStyle,
            }, ],
        },
    },
});

// ================== employee by age =================//
var empChartData = <?php echo $empChartData ?>;
var $visitorsChart = $("#visitors-chart");
var visitorsChart = new Chart($visitorsChart, {
    data: {
        labels: ["1-17", "18-25", "26-35", "36-45"],
        datasets: [{
                type: "line",
                data: empChartData,
                backgroundColor: "transparent",
                borderColor: "#007bff",
                pointBorderColor: "#007bff",
                pointBackgroundColor: "#007bff",
                fill: false,
            },
            // {
            //     type: "line",
            //     data: [60, 80, 70, 67, 80, 77, 100],
            //     backgroundColor: "tansparent",
            //     borderColor: "#ced4da",
            //     pointBorderColor: "#ced4da",
            //     pointBackgroundColor: "#ced4da",
            //     fill: false,
            // },
        ],
    },
    options: {
        maintainAspectRatio: false,
        tooltips: {
            mode: mode,
            intersect: intersect
        },
        hover: {
            mode: mode,
            intersect: intersect
        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                gridLines: {
                    display: true,
                    lineWidth: "4px",
                    color: "rgba(0, 0, 0, .2)",
                    zeroLineColor: "transparent",
                },
                ticks: $.extend({
                        beginAtZero: true,
                        callback: function(value) {
                            if (value >= 1000) {
                                value /= 1000;
                                value += "k";
                            }
                            return "នាក់" + value;
                        }
                    },
                    ticksStyle
                ),
            }, ],
            xAxes: [{
                display: true,
                gridLines: {
                    display: false
                },
                ticks: ticksStyle,
            }, ],
        },
    },
});

// ================== employee by gender =================//
var employeByGenderData = <?php echo $employeByGenderData ?>;
var $employeeGender = $("#employee-by-gender");
var employeeByGender = new Chart($employeeGender, {
    type: "bar",
    data: {
        labels: ["1-17", "18-25", "26-35", "36-45"],
        datasets: [{
                backgroundColor: "#007bff",
                borderColor: "#007bff",
                data: employeByGenderData.male,
            },
            {
                backgroundColor: "#ced4da",
                borderColor: "#ced4da",
                data: employeByGenderData.female,

            },
            // {
            //     backgroundColor: "#930293",
            //     borderColor: "#ced4da",
            //     data: employeByGenderData.female,

            // },
        ],
    },
    options: {
        maintainAspectRatio: false,
        tooltips: {
            mode: mode,
            intersect: intersect
        },
        hover: {
            mode: mode,
            intersect: intersect
        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                gridLines: {
                    display: true,
                    lineWidth: "4px",
                    color: "rgba(0, 0, 0, .2)",
                    zeroLineColor: "transparent",
                },
                ticks: $.extend({
                        beginAtZero: true,
                        callback: function(value) {
                            if (value >= 1000) {
                                value /= 1000;
                                value += "k";
                            }
                            return "នាក់" + value;
                        },
                    },
                    ticksStyle
                ),
            }, ],
            xAxes: [{
                display: true,
                gridLines: {
                    display: false
                },
                ticks: ticksStyle,
            }, ],
        },
    },
});
</script>
@endsection