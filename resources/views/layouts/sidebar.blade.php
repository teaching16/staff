  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('assets/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <!-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         
          <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link" id="menu_dashboard">
              <i class="nav-icon fas fa-th"></i>
              <p>
                {{__('t.Dashboard')}}
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('employee.index')}}" class="nav-link" id="menu_employee">
              <i class="nav-icon fas fa-users"></i>
              <p>
                {{__('t.Employee')}}
              </p>
            </a>
          </li>
          <li class="nav-item" id="menu_report">
            <a href="#" class="nav-link" >
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Reports
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('report.list_employee')}}?per_page=5" class="nav-link" id="menu_report_list_meployee">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List Employee</p>
                </a>
              </li>
            </ul>
          </li>

           <li class="nav-item" id="menu_setting">
            <a href="#" class="nav-link" >
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Settings
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('language.greeting_create_en')}}" class="nav-link" id="menu_greeting_en">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Greeting En</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('language.greeting_kh')}}" class="nav-link" id="menu_greeting_en">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Greeting Kh</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('user.list')}}" class="nav-link" id="menu_user">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('role.index')}}" class="nav-link" id="menu_role">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Role</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('permission.index')}}" class="nav-link" id="menu_permission">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permission (Dev Only)</p>
                </a>
              </li>
              @if(check_permission('province', 'view'))
              <li class="nav-item">
                <a href="{{route('province.index')}}" class="nav-link" id="menu_province">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Provinces</p>
                </a>
              </li>
              @endif
              <li class="nav-item">
                <a href="{{route('district.index')}}" class="nav-link" id="menu_district">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Districts</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('commune.index')}}" class="nav-link" id="menu_commune">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Communes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('village.index')}}" class="nav-link" id="menu_village">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Villages</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{route('user.logout')}}"> <i class="fa fa-signin-alt"></i> Logout {{auth()->user()->role_id}}</a>
          </li>
  
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>