<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/user/logout', [App\Http\Controllers\UserController::class, 'logout'])->name('user.logout');
Route::get('/swtich-lang/{lang}', [App\Http\Controllers\LanguageController::class, 'switchLang'])->name('language.change');

Route::get('/greeting/create-en', [App\Http\Controllers\LanguageController::class, 'createGreeting'])->name('language.greeting_create_en');
Route::post('/greeting/save/en', [App\Http\Controllers\LanguageController::class, 'greetingEn'])->name('language.greeting_en');
Route::get('/greeting/kh', [App\Http\Controllers\LanguageController::class, 'greetingKh'])->name('language.greeting_kh');
Route::post('/greeting/kh/save', [App\Http\Controllers\LanguageController::class, 'greetingkhSave'])->name('language.greeting_kh_save');

Route::group(['middleware' => ['auth', 'useraction']], function(){
    Route::get('/', function () {
        return view('welcome');
    });

    // dashbaord 
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('get-chart-data', [App\Http\Controllers\HomeController::class, 'getChartData'])->name('home.get_cart_data');


    // employee 
    Route::get('employee', [App\Http\Controllers\EmployeeController::class, 'index'])->name('employee.index');
    Route::get('employee/create', [App\Http\Controllers\EmployeeController::class, 'create'])->name('employee.create');
    Route::post('employee/save', [App\Http\Controllers\EmployeeController::class, 'save'])->name('employee.save');
    Route::get('employee/edit/{id}', [App\Http\Controllers\EmployeeController::class, 'edit'])->name('employee.edit');
    Route::post('employee/update', [App\Http\Controllers\EmployeeController::class, 'update'])->name('employee.update');

    // employee work experience
    Route::get('employee/experience', [App\Http\Controllers\EmployeeController::class, 'workExperience'])->name('employee.work_experience');
    // employee family info
    Route::get('employee/family-info', [App\Http\Controllers\EmployeeController::class, 'familyInfo'])->name('employee.family_info');

    
    // users
    Route::get('/user/list', [App\Http\Controllers\UserController::class, 'index'])->name('user.list');
    Route::post('/user/save', [App\Http\Controllers\UserController::class, 'save'])->name('user.save');
    Route::post('/user/update', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
    
    Route::get('/village/list', [App\Http\Controllers\VillageController::class, 'index'])->name('village.index');
    Route::post('/village/save', [App\Http\Controllers\VillageController::class, 'save'])->name('village.save');
    Route::get('/village/edit/{id}', [App\Http\Controllers\VillageController::class, 'edit'])->name('village.edit');
    Route::post('/village/update', [App\Http\Controllers\VillageController::class, 'update'])->name('village.update');
    Route::get('/village/delete/{id}', [App\Http\Controllers\VillageController::class, 'delete'])->name('village.delete');
   
    // commune 
    Route::get('/commune/list', [App\Http\Controllers\CommuneController::class, 'index'])->name('commune.index');
    Route::get('/commune/edit/{id}', [App\Http\Controllers\CommuneController::class, 'edit'])->name('commune.edit');

    // district
    Route::get('/district/list', [App\Http\Controllers\DistrictController::class, 'index'])->name('district.index');
    Route::get('/district/edit/{id}', [App\Http\Controllers\DistrictController::class, 'edit'])->name('district.edit');

    // province
    Route::get('/province/list', [App\Http\Controllers\ProvinceController::class, 'index'])->name('province.index');
    
    //Role
    Route::get('/role/list', [App\Http\Controllers\RoleController::class, 'index'])->name('role.index');
    // permission 
    Route::get('/permission/list', [App\Http\Controllers\PermissionController::class, 'index'])->name('permission.index');
    // permission feature
    Route::get('/permission-feature/list/{id}', [App\Http\Controllers\PermissionFeatureController::class, 'index'])->name('permission_feature.index');
    // role permision
    Route::get('/role-permission/list/{roel_id}', [App\Http\Controllers\RolePermissionController::class, 'index'])->name('role_permission.index');
    Route::get('/role-permission/regenerate', [App\Http\Controllers\RolePermissionController::class, 'reGenerateRole'])->name('role_permission.regenerate');
    // save role permission 
    Route::post('/role-psermission/save', [App\Http\Controllers\RolePermissionController::class, 'savePermission'])->name('role_permission.save');


    Route::get('/district-by-id', [App\Http\Controllers\DistrictController::class, 'getDistrictById'])->name('district.get_by_province_id');
    Route::get('/commune-by-id', [App\Http\Controllers\CommuneController::class, 'getCommuneById'])->name('commune.get_by_district_id');
    Route::get('/village-by-id', [App\Http\Controllers\VillageController::class, 'getVillageById'])->name('village.get_by_commune_id');


    // base actoins 
    Route::post('/base-action/save', [App\Http\Controllers\BaseController::class, 'save'])->name('base_action.save');
    Route::get('/base-action/edit', [App\Http\Controllers\BaseController::class, 'edit'])->name('base_action.edit');
    Route::post('/base-action/update', [App\Http\Controllers\BaseController::class, 'update'])->name('base_action.update');
    Route::get('/base-action/delete', [App\Http\Controllers\BaseController::class, 'delete'])->name('base_action.delete');

    Route::group(['prefix'=>'report','as'=>'report.'], function(){
        Route::get('/list-employee', [App\Http\Controllers\ReportController::class, 'listEmployee'])->name('list_employee');
    });
});

Route::get('/no-permission', function(){
    echo "You don't have permissoin!";
})->name('no-permission');

