<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('test', function(){
    return response()->json(
        [
            'msm' => 'Success',
            'data' => 'Hello',
        ]
    );
});

Route::middleware('checkapikey')->group(function(){
    Route::post('/login', [App\Http\Controllers\Api\AuthController::class, 'login']);
    Route::middleware('auth:api')->group(function(){
        Route::post('/logout', [App\Http\Controllers\Api\AuthController::class, 'logout']);
        Route::post('/province1/save', [App\Http\Controllers\Api\ProvinceController::class, 'save']);
        Route::get('/province1/detail/{id}', [App\Http\Controllers\Api\ProvinceController::class, 'detail']);
        Route::post('/province1/update', [App\Http\Controllers\Api\ProvinceController::class, 'update']);
        Route::post('/province1/delete', [App\Http\Controllers\Api\ProvinceController::class, 'delete']);
    });

    Route::get('/province/list', [App\Http\Controllers\Api\ProvinceController::class, 'list']);
    Route::post('/province/save', [App\Http\Controllers\Api\ProvinceController::class, 'save']);
    Route::get('/province/detail/{id}', [App\Http\Controllers\Api\ProvinceController::class, 'detail']);
    Route::post('/province/update', [App\Http\Controllers\Api\ProvinceController::class, 'update']);
    Route::post('/province/delete', [App\Http\Controllers\Api\ProvinceController::class, 'delete']);
});