<?php 

function reserveDate($date){
    $data = date('Y-m-d', strtotime($data));
    $d = explode('-', $date);
    return $d['2'].'-'.$d['1'].'-'.$d['0'];
}

function get_day_kh($date){
    $days = [
        "0" => "០០",
        "01" => "០១",
        "02" => "០២",
        "03" => "០៣",
        "04" => "០៤",
        "05" => "០៥",
        "06" => "០៦",
        "07" => "០៧",
        "08" => "០៨",
        "09" => "០៩",
        "10" => "១០",
        "11" => "១១",
        "12" => "១២",
        "13" => "១៣",
        "14" => "១៤",
        "15" => "១៥",
        "16" => "១៦",
        "17" => "១៧",
        "18" => "១៨",
        "19" => "១៩",
        "20" => "២០",
        "21" => "២១",
        "22" => "២២",
        "23" => "២៣",
        "24" => "២៤",
        "25" => "២៥",
        "26" => "២៦",
        "27" => "២៧",
        "28" => "២៨",
        "29" => "២៩",
        "30" => "៣០",
        "31" => "៣១",
    ];

    // ចាប់ពីថ្ងៃទី ១៤ ដល់ ៣០ ខែតុលា ២០២២

    $dates = explode('-', $date);

    return $days[$dates[2]];
}
// argument format  'Y-m-d'
function get_month_kh($date){
    $months = [
        '0' => '០', 
        '01' =>'មករា', 
        '02' =>'កុម្ភៈ', 
        '03' =>'មីនា', 
        '04' => 'មេសា', 
        '05' =>'ឧសភា', 
        '06' =>'មិថុនា', 
        '07' =>'កក្កដា', 
        '08' =>'សីហា', 
        '09' =>'កញ្ញា', 
        '10' =>'តុលា', 
        '11' =>'វិច្ឆិកា', 
        '12' =>'ធ្នូ'
    ];
    $dates = explode('-', $date);
    return $months[$dates[1]];
    
}

// argument format  'Y-m-d'
function get_year_kh($date){
    $dates = explode('-', $date);
    $year = array_map('intval', str_split($dates[0]));
    $numbers = [
        "0" => "០",
        "1" => "១",
        "2" => "២",
        "3" => "៣",
        "4" => "៤",
        "5" => "៥",
        "6" => "៦",
        "7" => "៧",
        "8" => "៨",
        "9" => "៩",
    ];

    return $numbers[$year[0]] . $numbers[$year[1]] . $numbers[$year[2]] . $numbers[$year[3]];
}
// argument format  'Y-m-d'
function get_full_date_kh($date){
    $day = get_day_kh($date);
    $month = get_month_kh($date);
    $year = get_year_kh($date);
    return $day.' '. $month .' '. $year;
}

function translateDate($lang = 'en'){
    $date = date('Y-m-d');
    if($lang == 'kh'){
        return get_full_date_kh($date);
    }
    return $date;
}

function tran($en, $kh, $lang = 'en'){
    if($lang == 'kh'){
        return $kh;
    }
    return $en;
}

// check permission 
function btn_delete($tbl, $permission, $action, $id){
    $btn_delete = '';
    $user_role_id = auth()->user()->role_id;
    // param permission, action
    $feature_id = DB::table('permission_features')
        ->join('permissions', 'permissions.id', 'permission_id')
        ->where('permissions.key', $permission)
        ->where('permission_features.aleas', $action)
        ->select('permission_features.id')
        ->first()->id;
    $role_permission = DB::table('role_permissions')
        ->join('permissions', 'permissions.id', 'permission_id')
        ->where('permissions.key', $permission)
        ->where('role_permissions.role_id', $user_role_id)
        ->first();
    if($role_permission->permisions && in_array($feature_id, json_decode($role_permission->permisions))){
        $btn_delete = '<a class="btn btn-danger btn-sm" tbl="'.$tbl.'" key="id" onclick="deleteRecord('.$id.', this)"><i class="fa fa-trash"></i></a>' ;
    }

    return $btn_delete;
}

function btn_edit($tbl, $permission, $action, $id, $editFunction = ''){
    $btn_edit = '';
    $user_role_id = auth()->user()->role_id;
    // param permission, action
    $feature_id = DB::table('permission_features')
        ->join('permissions', 'permissions.id', 'permission_features.permission_id')
        ->where('permission_features.aleas', $action)
        ->where('permissions.key', $permission)
        ->select('permission_features.id')
        ->first()->id;
    $role_permission = DB::table('role_permissions')
        ->join('permissions', 'permissions.id', 'permission_id')
        ->where('permissions.key', $permission)
        ->where('role_permissions.role_id', $user_role_id)
        ->first();
    if($role_permission->permisions && in_array($feature_id, json_decode($role_permission->permisions))){
        if($editFunction){
            $btn_edit = '<a class="btn btn-info btn-sm" onclick="'.$editFunction.'"><i class="fa fa-edit"></i></a>' ;
        }else{
            $btn_edit = '<a class="btn btn-info btn-sm" onclick="edit('.$id.',  this)"><i class="fa fa-edit"></i></a>' ;

        }
    }

    return $btn_edit;
}

function check_permission($permission, $action){
    $user_role_id = auth()->user()->role_id;

    // param permission, action
    $feature = DB::table('permission_features')
        ->join('permissions', 'permissions.id', 'permission_features.permission_id')
        ->where('permission_features.aleas', $action)
        ->where('permissions.key', $permission)
        ->select('permission_features.id')
        ->first();
    $role_permission = DB::table('role_permissions')
        ->join('permissions', 'permissions.id', 'permission_id')
        ->where('permissions.key', $permission)
        ->where('role_permissions.role_id', $user_role_id)
        ->first();
    
    if($role_permission->permisions && in_array($feature->id, json_decode($role_permission->permisions))){
        return 1;
    }
    
    return 0;

}

function getImage($file_name){
    // return asset('images/'.$file_name);
    $disk = Storage::disk('space');
    if($file_name != ''){
        return $disk->url($file_name);
    }
    return '';
}

function saveFile($file, $path) {
    $disk = Storage::disk('space');
    $disk->put($path, file_get_contents($file));
    $disk->setVisibility($path, 'public');
}



