<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $roles = DB::table('roles')
            ->leftJoin('users', 'roles.id', 'users.role_id')
            ->where('roles.active', 1)
            ->select(
                'roles.name',
                DB::raw("COUNT(users.id) as total_user"),
                DB::raw("COUNT(IF(users.is_default=1, 1, NULL)) as total_male"),
                DB::raw("COUNT(IF(users.is_default=0, 1, NULL)) as total_female")
            )
            ->groupBy('roles.id')
            ->get();
        $total = DB::table('users')->count();
        return view('test', ['roles'=> $roles, 'total'=>$total]);
        exit();
        
        $data['users'] = DB::table('users')->where('active', 1)->count();
        $data['employees'] = DB::table('employees')->where('active', 1)->count();
        $dateS = Carbon::now()->subDays(3)->format('Y-m-d');
        $data['new_employees'] = DB::table('employees')
            ->whereDate('created_at', '>', $dateS)
            ->where('active', 1)->count();
        
        $saleChartData = $this->getChartData();
        $data['saleChartData'] = json_encode($saleChartData);
        $data['saleChartTotal'] = $saleChartData['total'];

        $data['empChartData'] = json_encode($this->getEmployeeChartData());
        $data['employeByGenderData'] = json_encode($this->getEmpByGender());

        return view('home', $data);
    }

    public function getChartData(){
        $roles = DB::table('roles')->where('active', 1)->get();
        $role_result = [];
        $user_in_role_result = [];
        foreach($roles as $role){
            array_push($role_result, $role->name);
            $count_user = DB::table('users')->where('active', 1)->where('role_id', $role->id)->count();
            array_push($user_in_role_result, $count_user);

        }
        $total = DB::table('users')->where('active', 1)->count();
        return ['label' => $role_result, 'data' => $user_in_role_result, 'total' => $total];
    }

    public function getEmployeeChartData(){
        //1-17, 18-25, 26-35,
        $age1_17 = [$this->reverse_birthday(1), $this->reverse_birthday(17)];
        $age18_25 = [$this->reverse_birthday(18), $this->reverse_birthday(25)];
        $age26_35 = [$this->reverse_birthday(26), $this->reverse_birthday(35)];
        $age36_45 = [$this->reverse_birthday(36), $this->reverse_birthday(55)];
        // dd($age1_17);
        $emp = DB::table('employees')->where('active', 1)
            ->select(
                DB::raw("COUNT(IF(dob <= '$age1_17[0]' AND dob >= '$age1_17[1]', 1, NULL)) as age1_17"),
                DB::raw("COUNT(IF(dob <= '$age18_25[0]' AND dob >= '$age18_25[1]', 1, NULL)) as age18_25"),
                DB::raw("COUNT(IF(dob <= '$age26_35[0]' AND dob >= '$age26_35[1]', 1, NULL)) as age26_35"),
                DB::raw("COUNT(IF(dob <= '$age36_45[0]' AND dob >= '$age36_45[1]', 1, NULL)) as age36_45"),
            )->first();

        // $emp1 = DB::table('employees')->where('active', 2)->whereDate('dob', '<', $this->reverse_birthday(49))->get();
        
        
        $employeeData = [];
        foreach($emp as $e){
            array_push($employeeData, $e);
        }

        return $employeeData;


      
    }

    public function getEmpByGender(){
        //1-17, 18-25, 26-35,
        $age1_17 = [$this->reverse_birthday(0), $this->reverse_birthday(17)];
        $age18_25 = [$this->reverse_birthday(18), $this->reverse_birthday(25)];
        $age26_35 = [$this->reverse_birthday(26), $this->reverse_birthday(35)];
        $age36_45 = [$this->reverse_birthday(36), $this->reverse_birthday(55)];
        $empMale = DB::table('employees')->where('active', 1)->where('gender', 1)
            ->select(
                DB::raw("COUNT(IF(dob <= '$age1_17[0]' AND dob >= '$age1_17[1]', 1, NULL)) as age1_17"),
                DB::raw("COUNT(IF(dob <= '$age18_25[0]' AND dob >= '$age18_25[1]', 1, NULL)) as age18_25"),
                DB::raw("COUNT(IF(dob <= '$age26_35[0]' AND dob >= '$age26_35[1]', 1, NULL)) as age26_35"),
                DB::raw("COUNT(IF(dob <= '$age36_45[0]' AND dob >= '$age36_45[1]', 1, NULL)) as age36_45"),
            )->first();

        $empFemale = DB::table('employees')->where('active', 1)->where('gender', 2)
            ->select(
                DB::raw("COUNT(IF(dob <= '$age1_17[0]' AND dob >= '$age1_17[1]', 1, NULL)) as age1_17"),
                DB::raw("COUNT(IF(dob <= '$age18_25[0]' AND dob >= '$age18_25[1]', 1, NULL)) as age18_25"),
                DB::raw("COUNT(IF(dob <= '$age26_35[0]' AND dob >= '$age26_35[1]', 1, NULL)) as age26_35"),
                DB::raw("COUNT(IF(dob <= '$age36_45[0]' AND dob >= '$age36_45[1]', 1, NULL)) as age36_45"),
            )->first();


        // $emp1 = DB::table('employees')->where('active', 2)->whereDate('dob', '<', $this->reverse_birthday(49))->get();
        
        
        $employeeMaleData = [];
        $employeeFemaleData = [];
        foreach($empMale as $e){
            array_push($employeeMaleData, $e);
        }
        foreach($empFemale as $e){
            array_push($employeeFemaleData, $e);
        }

        return ['male' => $employeeMaleData, 'female' => $employeeFemaleData];


      
    }

    public function reverse_birthday( $years ){
        return date('Y-m-d', strtotime($years . ' years ago'));
    }

}