<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request){
        $valilator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($valilator->fails()){
            return response()->json([
                'data' => 'error',
                'status' => 500,
                'msm' => 'Invalid email or password!'
            ]);
        }

        $user = User::where('email', $request->email)->first();
        if(!User::where('email', $request->email)->count()){
            return response()->json([
                'data' => 'error',
                'status' => 404,
                'msm' => 'Account not exist!'
            ]);
        }elseif($user->active == 0){
            return response()->json([
                'data' => 'error',
                'status' => 404,
                'msm' => 'Your account was blocked!'
            ]);
        }elseif(!Hash::check($request->password, $user->password)){
            return response()->json([
                'data' => 'error',
                'status' => 404,
                'msm' => 'Invalid password!'
            ]);
        }else{
            $attempt_data = [
                'email' => $request->email,
                'password' => $request->password,
            ];
            if(auth()->attempt($attempt_data)){

                $auth = auth()->user()->createToken('Secret');
                $token = $auth->accessToken;
                return response()->json([
                    'data' => ['user_data' => auth()->user(), 'token' => $token],
                    'status' => 200,
                    'msm' => 'Login Success!'
                ]);
            }else{
                return response()->json([
                    'data' => 'error',
                    'status' => 404,
                    'msm' => 'Invalid email or password!'
                ]);
            }
           
        }
    }

    public function logout(Request $request){
        
        // if(auth()->check()){
        //     return auth()->user()->id;
        // }
        $user = auth()->user()->token();
        $user->revoke();
        return response()->json([
            'data' => 'success',
            'status' => 200,
            'msm' => 'You have logged out successfully.'
        ]);
    }
}
