<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ProvinceController extends Controller
{
    public function list(Request $request){
       
        $provinces = DB::table('provinces')->where('active', 1)->get();
        return response()->json([
            'data' => $provinces,
            'status' => 200,
            'msm' => 'Get list successfully.'
        ]);
    }

    public function save(Request $request){
        if(!isset($request->name) || $request->name == '' ){
            return response()->json([
                'data' => 'error',
                'status' => 500,
                'msm' => 'Name is required!'
            ]);
        }
        $data = [
            'name' => $request->name,
            'name_en' => $request->name_en
        ];
        $id = DB::table('provinces')->insertGetId($data);
        return response()->json([
            'data' => $id,
            'status' => 200,
            'msm' => 'Insert successfully.'
        ]);
    }

    public function update(Request $request){
        if(!isset($request->name) || $request->name == '' ){
            return response()->json([
                'data' => 'error',
                'status' => 500,
                'msm' => 'Name is required!'
            ]);
        }
        $data = [
            'name' => $request->name,
            'name_en' => $request->name_en
        ];
        DB::table('provinces')->where('id', $request->id)->update($data);
        return response()->json([
            'data' => $data,
            'status' => 200,
            'msm' => 'Update successfully.'
        ]);
    }

    public function detail($id){
        $data = DB::table('provinces')->find($id);
        return response()->json([
            'data' => $data,
            'status' => 200,
            'msm' => 'success'
        ]);
    }

    public function delete(Request $request){
        if(!isset($request->id) || $request->id == '' ){
            return response()->json([
                'data' => 'error',
                'status' => 500,
                'msm' => 'Invalid argument!'
            ]);
        }
        DB::table('provinces')->where('id', $request->id)->update(['active' => 0]);
        return response()->json([
            'data' => '',
            'status' => 200,
            'msm' => 'Deleted successfully.'
        ]);
    }
}
