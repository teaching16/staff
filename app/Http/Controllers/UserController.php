<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use App\Models\User;
use Auth;


class UserController extends Controller
{

    public function index(Request $req){
        
        
       if($req->ajax()){
        $data = DB::table('users')
            ->join('roles', 'roles.id', 'users.role_id')
            // ->where('users.created_by', auth()->user()->id)
            ->where('users.active', 1)
            ->select(
                'users.*',
                'roles.name as role_name',
                // DB::raw("CONCAT(users.first_name, ' ', users.last_name) as full_name"),
            );
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('custom_name', function($row){
                // $user = DB::table('users')->where('id', $row->id)->get();
                // return $user[0]->first_name.' '. $user[0]->last_name;

                // $user = DB::table('users')->where('id', $row->id)->first();
                // $user = DB::table('users')->find($row->id);
                $user = User::find($row->id);
                return $user->first_name.' '.$user->last_name;

            })
            ->addColumn('action', function($row){
                $btn_edit = btn_edit('users', 'user', 'update', $row->id);
                $btn_delete = btn_delete('users', 'user', 'delete', $row->id);
                $btns = $btn_edit.' '.$btn_delete;
                return $btns;
            })
            ->addColumn('photo', function($row){
                $img_url = getImage($row->photo);
                $photo = "<img src='$img_url' width='50px'>";
                return $photo;
            })
            ->addColumn('is_default', function($row){
                $is_checked = $row->is_default ? 'checked' : '';
                $checkbox = "<input type='checkbox' value='$row->id' onchange='setDefault($row->id, this)' $is_checked>";
                return $checkbox;
            })
            ->rawColumns(['action', 'custom_name', 'photo', 'is_default'])
            ->make(true);
       }

       $data['roles'] = DB::table('roles')->where('active', 1)->get();
       return view('users.index', $data);
    }

    public function save(Request $req){
        if(User::where('email', $req->email)->count()){
            return redirect()->back()->with('error', 'Email aready exist!');
        }
        $user = new User;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->role_id = $req->role_id;
        $user->created_by = auth()->user()->id;
        $user->password = Hash::make($req->password);

        if($req->hasFile('photo')){ // check have or not
            $file = $req->file('photo'); // get file object
            $name = strtotime('now').rand(0, 9999).'.'.$file->getClientOriginalExtension(); // get file extention
            $destination = public_path('/images');
            $file->move($destination, $name); // upload
            $user->photo = $name; // set file name in database
        }

        if($user->save()){
            return redirect()->back()->with('success', 'User created successfully.');
        }
        return redirect()->back()->with('error', 'Somthing went wrong!');
    }

    public function update(Request $req){
        // if(User::where('email', $req->email)->count()){
        //     return redirect()->back()->with('error', 'Email aready exist!');
        // }
        $user =  User::find($req->id);
        $user->name = $req->name;
        $user->email = $req->email;
        $user->role_id = $req->role_id;
       
        if($req->password){
            $user->password = Hash::make($req->password);
        }

        if($req->hasFile('photo')){ // check have or not
            $file = $req->file('photo'); // get file object
            $name = strtotime('now').rand(0, 9999).'.'.$file->getClientOriginalExtension(); // get file extention
            $destination = public_path('/images');
            $file->move($destination, $name); // upload
            $user->photo = $name; // set file name in database
        }

        if($user->save()){
            return redirect()->back()->with('success', 'User created successfully.');
        }
        return redirect()->back()->with('error', 'Somthing went wrong!');
    }

    public function logout(){
        if(Auth::logout()){
            return redirect()->route('login');
        }
        return redirect()->back();
    }
    
}
